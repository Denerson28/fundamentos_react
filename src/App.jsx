import './App.css';
import React from 'react';


import Contador from './components/accountant/Cont';
import Input from './components/formulary/Input';
import IndiretaPai from './components/comunication/IndiretaPai';
import DiretaPai from './components/comunication/DiretaPai';
import UsuarioInfo from './components/conditions/UsuarioInfo';
import ParOuImpar from './components/conditions/ParOuImpar';
import ProductsList from './components/loops/ProductsList';
import StudentList from './components/loops/StudentList';
import Family from './components/basics/Family';
import MemberFamily from './components/basics/MemberFamily';
import Card from './components/layout/Card';
import Random from './components/basics/Random';
import Fragment from './components/basics/Fragment';
import WithParams from './components/basics/WithParams';
import First from './components/basics/First';

export default function App(){

    return (
        <div className="App">
            <h1>Fundamentos React</h1>

            <div className="Cards">

                <Card titulo="#12 Contador" color="#979A9A">
                    <Contador numeroInicial={10}></Contador>
                </Card>

                <Card titulo="#11 Componente Controlado" color="#4A235A">
                    <Input></Input>
                </Card>
                <Card titulo="#10 Comunicação indireta" color="#1F618D">
                    <IndiretaPai></IndiretaPai>
                </Card>

                <Card titulo="#9 Comunicação direta" color="#5F6A6A">
                    <DiretaPai></DiretaPai>
                </Card>

                <Card titulo="#8 Par ou Impar" color="#A93226">
                    <ParOuImpar numero={20}></ParOuImpar>
                    <UsuarioInfo usuario={{name: "Denerson"}}></UsuarioInfo>
                    {/* <UsuarioInfo usuario={{}}></UsuarioInfo> */}
                </Card>

                <Card titulo="#7 Tabela de produtos" color="#F1C40F">
                    <ProductsList></ProductsList>
                </Card>
                
                <Card titulo="#6 Repetição" color="#212F3C">
                    <StudentList></StudentList>
                </Card>
                <Card titulo="#5 Componente com Filho" color="#117864">
                    <Family surname="Ferreira">
                        <MemberFamily name="Pedro"></MemberFamily>
                        <MemberFamily name="Denerson"></MemberFamily>
                        <MemberFamily name="Gabi"></MemberFamily>
                    </Family>
                </Card>

                <Card titulo="#4 Desafio Aleatório" color="#7FB3D5">
                    <Random min={1} max={100}></Random>
                </Card>

                <Card titulo="#3 Fragmento" color="#8E44AD">
                    <Fragment />
                </Card>

                <Card titulo="#2 Com Parametro" color="#229954">
                    <WithParams
                        titulo="Situação do aluno"
                        aluno="Denerson"
                        nota={9.5}>
                    </WithParams>
                </Card>

                <Card titulo="#1 Primeiro Componente" color="#515A5A">
                    <First></First>
                </Card>
            </div>

        </div>
    );
}