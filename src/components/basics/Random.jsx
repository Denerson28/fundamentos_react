import React from 'react';

export default function Random(props){

    const min = props.min;
    const max = props.max;
    
    return (
        <div>
            <p>valor de min: {min}</p>
            <p>valor de max: {max}</p>
            <p>Numero aleatório entre {min} e {max}: {parseInt(Math.random() * (max - min) + min)}</p>
        </div>
    )
}