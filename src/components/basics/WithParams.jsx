import React from 'react'

export default function WithParams(params) {

    return (
        <div>
            <h2>{params.titulo}</h2>
            <p>O aluno {params.aluno} teve a nota {params.nota} e está {params.nota > 6 ? "aprovado" : "reprovado"}</p>
        </div>
    )
}