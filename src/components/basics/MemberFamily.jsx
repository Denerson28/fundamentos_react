import React from 'react';

export default function MemberFamily(props) {
    return (
        <div>
            <div> {props.name} <strong>{props.surname}</strong></div>
        </div>
    );
};