import React from 'react';
import If, { Else } from './If'


export default function UsuarioInfo(props) {

    const usuario = props.usuario || {}
    return (
        <div>
            <If teste={usuario && usuario.name}>
                Seja Bem Vindo <strong>{usuario.name}</strong>!

                <Else>
                    Seja Bem Vindo <strong>Anônimo</strong>!
                </Else>
            </If>
            
        </div>
    )
}