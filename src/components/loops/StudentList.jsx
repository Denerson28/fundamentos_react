import React from 'react'
import students from '../../data/students.js'

export default function StudentList(props) {
    const studentsJSX = students.map((student) => {
        return (
            <li key={student.id}>
                {student.id}) {student.name} - {student.note}
            </li>
        );
    });

    return (
        <div>
            <ul style={{listStyle: 'none'}}>
                {studentsJSX}
            </ul>
        </div>
    );
};