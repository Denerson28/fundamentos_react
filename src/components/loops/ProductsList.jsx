import React from 'react';
import './ProductsList.css';
import products from '../../data/products';


export default function ProductsList(props) {

        const productsJSX = products.map((product, i) => {
            i = product.id;

            return (
                <tr className={i % 2 === 0 ? 'Par' : ''} key={product.id}>
                <td>{product.id}</td>
                <td>{product.name}</td>
                <td>R$ {product.price.toFixed(2).replace('.', ',')}</td>
            </tr>
            );
        });

        return (
            <div className="tableProducts">
            <table >
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {productsJSX}
                </tbody>
            </table>
        </div>
        );
}