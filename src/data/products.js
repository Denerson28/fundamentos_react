const products = [
    {id: 1, name: "nescau", price: 5.99},
    {id: 2, name: "toddy", price: 8.99},
    {id: 3, name: "coca-cola", price: 5.99},
    {id: 4, name: "cereal", price: 8.50},
    {id: 5, name: "docinho", price: 7.00},
    {id: 6, name: "leite", price: 4.50},
    {id: 7, name: "suco", price: 6.00},
];

export default products