const students = [
    { id: 1, name: "Denerson", note: 9.2},
    { id: 2, name: "Jon", note: 5.5},
    { id: 3, name: "Mairon", note: 6.8},
    { id: 4, name: "Gabi", note: 7.4},
    { id: 5, name: "Luquinha", note: 6.5},
    { id: 6, name: "Renato", note: 4.2},
    { id: 7, name: "Daniel", note: 5.5},
    { id: 8, name: "Juju", note: 10},
];

export default students;